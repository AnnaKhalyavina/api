<?php

namespace App\Classes;

use Illuminate\Support\Facades\Http;

class OxfordDictionaryApi
{

    public function translate()
    {
        $lang = 'en';
        $word = 'ace';
        $app_id = '1e0f6373';
        $app_key = '6da9adfd84b6b43a9024d0d58856373e';
        $url = env('OXFORD_API_URL').'/'.'/en-us/ace';
        $response = Http::withHeaders(['app_id' => $app_id,
            'app_key' => $app_key])->get($url);
        dd($response->body());
    }

}
