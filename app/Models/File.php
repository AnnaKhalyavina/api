<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $fillable = [
        'id',
        'domain_id',
        'subject',
        'unisender_send_date_at',
        'created_at',
    ];

}
