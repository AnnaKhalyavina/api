<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {

        if (!$token = auth()->attempt(["email" => $request->email, "password" => $request->password])) {
            return response()->json([
                "message" => "Invalid email or password"
            ]);
        }

        return response()->json([
            "status" => 'success',
            "access_token" => $token,

        ]);

    }

    public function user()
    {
        $user_data = auth()->user();
        return response()->json([
            'user' => $user_data
        ]);
    }

}
