<?php

namespace App\Console\Commands;

use App\Models\File;
use Illuminate\Console\Command;

class ParseJson extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:json';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $items = $this->parseFile();
        $arrayToDelete = [];
        foreach ($items['data'] as $item) {
            $arrayToDelete[] = $item['id'];
            $value = [
                'id' => $item['id']
            ];
            $array = [
                'domain_id' => $item['domain_id'],
                'subject' => $item['subject'],
                'unisender_send_date_at' => $item['unisender_send_date_at'],
                'created_at' => $item['created_at']
            ];
            File::updateOrCreate($value, $array);
        }
        File::whereNotIn('id', $arrayToDelete)->delete();

    }

    private function parseFile(): array
    {
        $file = file_get_contents('test.json');
//        $file = file_get_contents('https://gist.githubusercontent.com/Quiss/14f705fde7499116eb710eba1ea28025/raw/796f4b8c42551749ac7dde02109ee21d93692074/test.json');
        return json_decode($file, true);

    }

}
