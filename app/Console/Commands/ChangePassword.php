<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class ChangePassword extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'password:change';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'change user password';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $email = $this->ask('What is your email?');
        $oldPassword = $this->secret('Your current password?');
        $hashedPassword = User::where('email', 'Anna@gmail.com')->get();
        foreach ($hashedPassword as $item) {
            $password = $item->password;
        }
        if (Hash::check($oldPassword, $password)) {
            $newPassword = $this->secret('enter new password');
            User::where('email', $email)
                ->update(['password' => Hash::make($newPassword)
                ]);
            $this->info('password has been changed successfully');
        } else {
            $this->error('email or password is wrong');
        }

    }
}
